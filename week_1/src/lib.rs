//! Week 1: Programming Assignment
//!
//! Strongly connected components of a directed graph

use std::collections::{HashMap, HashSet};
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::num::ParseIntError;
use std::path::Path;

type Node = u32;
type Edge = (Node, Node);

#[derive(Debug)]
pub struct Graph {
    adj_list: HashMap<Node, Vec<Node>>,
    adj_list_rev: HashMap<Node, Vec<Node>>,
    nodes: HashSet<Node>,
}

#[derive(Debug)]
pub enum ReadError {
    Io(io::Error),
    Empty(String),
    Parse(String),
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> Self { Self::Io(err) }
}

impl fmt::Display for ReadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Io(err) => write!(f, "IO error: {}", err),
            Self::Empty(msg) => write!(f, "Empty file: {}", msg),
            Self::Parse(msg) => write!(f, "Parse error: {}", msg),
        }
    }
}

impl Graph {
    fn new() -> Self {
        Self {
            adj_list: HashMap::new(),
            adj_list_rev: HashMap::new(),
            nodes: HashSet::new(),
        }
    }

    fn add_edge(&mut self, src: Node, dest: Node) {
        self.adj_list.entry(src).or_default().push(dest);
        self.adj_list_rev.entry(dest).or_default().push(src);
        self.nodes.insert(src);
        self.nodes.insert(dest);
    }

    pub fn from_edges(edges: &Vec<Edge>) -> Result<Self, String> {
        if edges.is_empty() {
            return Err("Cannot create empty graph".to_string());
        }
        let mut graph = Self::new();
        for &(src, dest) in edges {
            graph.add_edge(src, dest);
        }
        Ok(graph)
    }

    fn parse_line(line: &str) -> Result<Edge, String> {
        let parts: Vec<&str> = line.split_whitespace().collect();

        if parts.len() != 2 {
            return Err(
                "Line should contain exactly 2 integers separated by whitespace".to_string(),
            );
        }

        let int_1: Result<Node, ParseIntError> = parts[0].parse();
        let int_2: Result<Node, ParseIntError> = parts[1].parse();

        match (int_1, int_2) {
            (Ok(i1), Ok(i2)) => Ok((i1, i2)),
            (Err(e), _) => Err(format!("Error parsing first integer: {}", e)),
            (_, Err(e)) => Err(format!("Error parsing second integer: {}", e)),
        }
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self, ReadError> {
        let file = File::open(path)?;
        let metadata = file.metadata()?;

        if metadata.len() == 0 {
            return Err(ReadError::Empty(
                "Cannot create graph from empty file".to_string(),
            ));
        }

        let reader = BufReader::new(file);
        let mut graph = Self::new();

        for line in reader.lines().map(|l| l.unwrap()) {
            let (src, dest) = Self::parse_line(&line).map_err(ReadError::Parse)?;
            graph.add_edge(src, dest);
        }

        Ok(graph)
    }

    fn neighbors(&self, node: Node) -> Option<&Vec<Node>> { self.adj_list.get(&node) }

    fn neighbors_reverse(&self, node: Node) -> Option<&Vec<Node>> { self.adj_list_rev.get(&node) }

    fn kosaraju_first_pass(&self, node: Node, visited: &mut HashSet<Node>, stack: &mut Vec<Node>) {
        visited.insert(node);
        if let Some(neighbors) = self.neighbors(node) {
            for &neighbor in neighbors {
                if !visited.contains(&neighbor) {
                    self.kosaraju_first_pass(neighbor, visited, stack);
                }
            }
        }
        stack.push(node);
    }

    fn kosaraju_second_pass(
        &self,
        node: Node,
        visited: &mut HashSet<Node>,
        component: &mut Vec<Node>,
    ) {
        visited.insert(node);
        component.push(node);
        if let Some(neighbors) = self.neighbors_reverse(node) {
            for &neighbor in neighbors {
                if !visited.contains(&neighbor) {
                    self.kosaraju_second_pass(neighbor, visited, component);
                }
            }
        }
    }

    pub fn kosaraju(&self) -> Vec<Vec<Node>> {
        let mut visited: HashSet<Node> = HashSet::with_capacity(self.nodes.len());
        let mut stack: Vec<Node> = Vec::with_capacity(self.nodes.len());

        for &node in self.nodes.iter() {
            if !visited.contains(&node) {
                self.kosaraju_first_pass(node, &mut visited, &mut stack);
            }
        }
        visited.clear();

        let mut components: Vec<Vec<Node>> = Vec::new();

        while let Some(node) = stack.pop() {
            if visited.contains(&node) {
                continue;
            }
            let mut component: Vec<Node> = Vec::new();
            self.kosaraju_second_pass(node, &mut visited, &mut component);
            components.push(component);
        }

        components
    }

    pub fn top_component_sizes(&self) -> Vec<usize> {
        let components = self.kosaraju();
        let mut all_sizes: Vec<usize> = components.iter().map(|c| c.len()).collect();
        all_sizes.sort_unstable_by(|a, b| b.cmp(a));

        let mut sizes = vec![0; 5];

        for (i, size) in all_sizes.into_iter().enumerate().take(5) {
            sizes[i] = size;
        }

        sizes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_keys(map: &HashMap<Node, Vec<Node>>) -> Vec<Node> {
        let mut keys: Vec<Node> = map.keys().copied().collect();
        keys.sort_unstable();
        keys
    }

    #[test]
    fn test_from_edges() {
        let edges: Vec<Edge> = vec![
            (7, 1),
            (5, 6),
            (5, 3),
            (4, 6),
            (4, 3),
            (4, 2),
            (2, 7),
            (1, 4),
            (1, 3),
            (1, 2),
        ];
        let result = Graph::from_edges(&edges);
        assert!(result.is_ok());
        let graph = result.unwrap();

        let tails = get_keys(&graph.adj_list);
        let expected_tails: Vec<Node> = vec![1, 2, 4, 5, 7];
        assert_eq!(tails, expected_tails);

        assert_eq!(&graph.adj_list[&1], &vec![4, 3, 2]);
        assert_eq!(&graph.adj_list[&2], &vec![7]);
        assert_eq!(&graph.adj_list[&4], &vec![6, 3, 2]);
        assert_eq!(&graph.adj_list[&5], &vec![6, 3]);
        assert_eq!(&graph.adj_list[&7], &vec![1]);

        assert_eq!(graph.nodes.len(), 7);
        assert!((1..=7).all(|n| graph.nodes.contains(&n)));
    }

    #[test]
    fn test_from_edges_empty_list() {
        let edges: Vec<Edge> = vec![];
        let result = Graph::from_edges(&edges);
        assert!(result.is_err());
    }

    #[test]
    fn test_from_file() {
        let path = "text_files/test_1.txt";
        let result = Graph::from_file(path);
        assert!(result.is_ok());
        let graph = result.unwrap();

        let tails = get_keys(&graph.adj_list);
        let expected_tails: Vec<Node> = vec![1, 2, 4, 5, 7];
        assert_eq!(tails, expected_tails);

        assert_eq!(&graph.adj_list[&1], &vec![2, 3, 4]);
        assert_eq!(&graph.adj_list[&2], &vec![7]);
        assert_eq!(&graph.adj_list[&4], &vec![2, 3, 6]);
        assert_eq!(&graph.adj_list[&5], &vec![3, 6]);
        assert_eq!(&graph.adj_list[&7], &vec![1]);

        assert_eq!(graph.nodes.len(), 7);
        assert!((1..=7).all(|n| graph.nodes.contains(&n)));
    }

    #[test]
    fn test_from_file_non_existing_file() {
        let path = "text_files/non_existing_file.txt";
        let result = Graph::from_file(path);
        assert!(result.is_err());
        assert!(matches!(result.unwrap_err(), ReadError::Io(_)));
    }

    #[test]
    fn test_from_file_invalid_edge_list() {
        let path = "text_files/test_2.txt";
        let result = Graph::from_file(path);
        assert!(result.is_err());
        assert!(matches!(result.unwrap_err(), ReadError::Parse(_)));
    }

    #[test]
    fn test_from_file_empty_file() {
        let path = "text_files/test_3.txt";
        let result = Graph::from_file(path);
        assert!(result.is_err());
        assert!(matches!(result.unwrap_err(), ReadError::Empty(_)));
    }

    #[test]
    fn test_neighbors_reverse() {
        let edges: Vec<Edge> = vec![
            (7, 1),
            (5, 6),
            (5, 3),
            (4, 6),
            (4, 3),
            (4, 2),
            (2, 7),
            (1, 4),
            (1, 3),
            (1, 2),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let node: Node = 3;
        let opt = graph.neighbors_reverse(node);
        assert!(opt.is_some());

        let neighbors = opt.unwrap();
        let expected_neighbors: Vec<Node> = vec![5, 4, 1];
        assert_eq!(neighbors, &expected_neighbors);
    }

    #[test]
    fn test_neighbors_reverse_no_neighbor() {
        let edges: Vec<Edge> = vec![
            (7, 1),
            (5, 6),
            (5, 3),
            (4, 6),
            (4, 3),
            (4, 2),
            (2, 7),
            (1, 4),
            (1, 3),
            (1, 2),
        ];
        let graph = Graph::from_edges(&edges).unwrap();
        let node: Node = 5;
        let opt = graph.neighbors_reverse(node);
        assert!(opt.is_none());
    }

    #[test]
    fn test_first_pass() {
        let edges: Vec<Edge> = vec![
            (1, 2),
            (2, 3),
            (2, 5),
            (2, 6),
            (3, 4),
            (3, 7),
            (4, 3),
            (4, 8),
            (5, 1),
            (5, 6),
            (6, 7),
            (7, 6),
            (8, 4),
            (8, 7),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let node: Node = 1;
        let mut visited: HashSet<Node> = HashSet::new();
        let mut stack: Vec<Node> = Vec::new();
        graph.kosaraju_first_pass(node, &mut visited, &mut stack);

        let all_visited = (1..=8).all(|n| visited.contains(&n));
        assert!(all_visited);

        let expected_stack: Vec<Node> = vec![6, 7, 8, 4, 3, 5, 2, 1];
        assert_eq!(stack, expected_stack);
    }

    #[test]
    fn test_second_pass_1() {
        let edges: Vec<Edge> = vec![
            (1, 2),
            (2, 3),
            (2, 5),
            (2, 6),
            (3, 4),
            (3, 7),
            (4, 3),
            (4, 8),
            (5, 1),
            (5, 6),
            (6, 7),
            (7, 6),
            (8, 4),
            (8, 7),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let node: Node = 3;
        let mut visited: HashSet<Node> = HashSet::from([1, 2, 5]);
        let mut component: Vec<Node> = Vec::new();
        graph.kosaraju_second_pass(node, &mut visited, &mut component);

        let expected_visited: HashSet<Node> = HashSet::from([1, 2, 3, 4, 5, 8]);
        assert_eq!(visited, expected_visited);

        let expected_component: Vec<Node> = vec![3, 4, 8];
        assert_eq!(component, expected_component);
    }

    #[test]
    fn test_second_pass_2() {
        let edges: Vec<Edge> = vec![
            (6, 7),
            (6, 4),
            (5, 6),
            (4, 7),
            (4, 5),
            (3, 4),
            (2, 3),
            (2, 0),
            (1, 2),
            (0, 1),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let node: Node = 4;
        let mut visited: HashSet<Node> = HashSet::from([0, 1, 2, 3]);
        let mut component: Vec<Node> = Vec::new();
        graph.kosaraju_second_pass(node, &mut visited, &mut component);

        let expected_visited: HashSet<Node> = HashSet::from([0, 1, 2, 3, 4, 5, 6]);
        assert_eq!(visited, expected_visited);

        let expected_component: Vec<Node> = vec![4, 6, 5];
        assert_eq!(component, expected_component);
    }

    #[test]
    fn test_kosaraju_cyclic_graph() {
        let edges: Vec<Edge> = vec![(1, 2), (2, 3), (3, 1)];
        let graph = Graph::from_edges(&edges).unwrap();

        let components = graph.kosaraju();
        assert_eq!(components.len(), 1);

        let component = &components[0];
        assert!(component.contains(&1));
        assert!(component.contains(&2));
        assert!(component.contains(&3));
    }

    #[test]
    fn test_kosaraju_3_components() {
        let edges: Vec<Edge> = vec![
            (1, 2),
            (2, 3),
            (2, 5),
            (2, 6),
            (3, 4),
            (3, 7),
            (4, 3),
            (4, 8),
            (5, 1),
            (5, 6),
            (6, 7),
            (7, 6),
            (8, 4),
            (8, 7),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let mut components = graph.kosaraju();
        assert_eq!(components.len(), 3);

        for component in components.iter_mut() {
            component.sort_unstable();
        }

        let component_1: Vec<Node> = vec![1, 2, 5];
        assert!(components.contains(&component_1));
        let component_2: Vec<Node> = vec![3, 4, 8];
        assert!(components.contains(&component_2));
        let component_3: Vec<Node> = vec![6, 7];
        assert!(components.contains(&component_3));
    }

    #[test]
    fn test_kosaraju_4_components() {
        let edges: Vec<Edge> = vec![
            (6, 7),
            (6, 4),
            (5, 6),
            (4, 7),
            (4, 5),
            (3, 4),
            (2, 3),
            (2, 0),
            (1, 2),
            (0, 1),
        ];
        let graph = Graph::from_edges(&edges).unwrap();

        let mut components = graph.kosaraju();
        assert_eq!(components.len(), 4);

        for component in components.iter_mut() {
            component.sort_unstable();
        }

        let component_1: Vec<Node> = vec![0, 1, 2];
        assert!(components.contains(&component_1));
        let component_2: Vec<Node> = vec![3];
        assert!(components.contains(&component_2));
        let component_3: Vec<Node> = vec![4, 5, 6];
        assert!(components.contains(&component_3));
        let component_4: Vec<Node> = vec![7];
        assert!(components.contains(&component_4));
    }

    #[test]
    fn test_component_sizes_3_components() {
        let edges: Vec<Edge> = vec![
            (1, 2),
            (2, 3),
            (2, 5),
            (2, 6),
            (3, 4),
            (3, 7),
            (4, 3),
            (4, 8),
            (5, 1),
            (5, 6),
            (6, 7),
            (7, 6),
            (8, 4),
            (8, 7),
        ];
        let graph = Graph::from_edges(&edges).unwrap();
        let sizes = graph.top_component_sizes();
        let expected_sizes: Vec<usize> = vec![3, 3, 2, 0, 0];
        assert_eq!(sizes, expected_sizes);
    }

    #[test]
    fn test_component_sizes_4_components() {
        let edges: Vec<Edge> = vec![
            (6, 7),
            (6, 4),
            (5, 6),
            (4, 7),
            (4, 5),
            (3, 4),
            (2, 3),
            (2, 0),
            (1, 2),
            (0, 1),
        ];
        let graph = Graph::from_edges(&edges).unwrap();
        let sizes = graph.top_component_sizes();
        let expected_sizes: Vec<usize> = vec![3, 3, 1, 1, 0];
        assert_eq!(sizes, expected_sizes);
    }
}
