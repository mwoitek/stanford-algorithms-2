use num_traits::bounds::Bounded;
use rustc_hash::{FxHashMap, FxHashSet};
use std::cmp::Ordering;
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::num::ParseIntError;
use std::path::Path;
use week_3::min_heap::MinHeap;

#[derive(Debug)]
pub enum ReadError {
    Io(io::Error),
    Empty(String),
    Parse(String),
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> Self { Self::Io(err) }
}

impl fmt::Display for ReadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Io(err) => write!(f, "IO error: {}", err),
            Self::Empty(msg) => write!(f, "Empty file: {}", msg),
            Self::Parse(msg) => write!(f, "Parse error: {}", msg),
        }
    }
}

type Node = u32;
type Weight = u64;
type Dist = u64;

const ASSIGN_INFINITY: Dist = 1000000;
const ASSIGN_NODES: [Node; 10] = [7, 37, 59, 82, 99, 115, 133, 165, 188, 197];

#[derive(Debug, PartialEq)]
struct NodeWeightPair {
    node: Node,
    weight: Weight,
}

#[derive(Clone, Debug)]
struct NodeDistPair {
    node: Node,
    dist: Dist,
}

impl Bounded for NodeDistPair {
    fn min_value() -> Self { Self { node: Node::default(), dist: Dist::MIN } }

    fn max_value() -> Self { Self { node: Node::default(), dist: Dist::MAX } }
}

impl PartialEq for NodeDistPair {
    fn eq(&self, other: &Self) -> bool { self.dist == other.dist }
}

impl PartialOrd for NodeDistPair {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> { self.dist.partial_cmp(&other.dist) }
}

#[derive(Debug)]
pub struct Graph {
    adj_list: FxHashMap<Node, Vec<NodeWeightPair>>,
    nodes: FxHashSet<Node>,
}

impl Graph {
    fn new() -> Self {
        Self {
            adj_list: FxHashMap::default(),
            nodes: FxHashSet::default(),
        }
    }

    fn add_edge(&mut self, node: Node, pair: NodeWeightPair) {
        self.nodes.insert(node);
        self.nodes.insert(pair.node);
        self.adj_list.entry(node).or_default().push(pair);
    }

    fn parse_line(line: &str) -> Result<(Node, Vec<NodeWeightPair>), String> {
        let mut parts = line.trim().split('\t');

        let first_int_str = parts.next().ok_or("Missing first integer")?;
        let first_int: Node = first_int_str
            .parse()
            .map_err(|e: ParseIntError| format!("Failed to parse first integer: {}", e))?;

        let mut pairs = Vec::new();

        for part in parts {
            let mut ints = part.split(',');

            let i1_str = ints.next().ok_or("Missing first integer in pair")?;
            let i2_str = ints.next().ok_or("Missing second integer in pair")?;

            let i1: Node = i1_str.parse().map_err(|e: ParseIntError| {
                format!("Failed to parse first integer in pair: {}", e)
            })?;
            let i2: Weight = i2_str.parse().map_err(|e: ParseIntError| {
                format!("Failed to parse second integer in pair: {}", e)
            })?;

            pairs.push(NodeWeightPair { node: i1, weight: i2 });
        }

        Ok((first_int, pairs))
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self, ReadError> {
        let file = File::open(path)?;
        let metadata = file.metadata()?;

        if metadata.len() == 0 {
            return Err(ReadError::Empty(
                "Cannot create graph from empty file".to_string(),
            ));
        }

        let reader = BufReader::new(file);
        let mut graph = Self::new();

        for line in reader.lines().map(|l| l.unwrap()) {
            let (node, pairs) = Self::parse_line(&line).map_err(ReadError::Parse)?;
            for pair in pairs {
                graph.add_edge(node, pair);
            }
        }

        Ok(graph)
    }

    fn neighbors(&self, node: Node) -> impl Iterator<Item = &NodeWeightPair> {
        self.adj_list.get(&node).unwrap().iter()
    }

    pub fn dijkstra(&self, source: Node) -> FxHashMap<Node, Dist> {
        let mut visited: FxHashSet<Node> = FxHashSet::default();

        let mut dists: FxHashMap<Node, Dist> = self.nodes.iter().map(|&n| (n, Dist::MAX)).collect();
        dists.insert(source, 0);

        let mut queue: MinHeap<NodeDistPair> = MinHeap::new();
        queue.push(NodeDistPair { node: source, dist: 0 });

        while let Some(pair) = queue.pop_min() {
            let (node, min_dist) = (pair.node, pair.dist);
            visited.insert(node);

            // Ignore stale entries in the priority queue
            if dists[&node] < min_dist {
                continue;
            }

            for neighbor_pair in self.neighbors(node) {
                if visited.contains(&neighbor_pair.node) {
                    continue;
                }

                let (neighbor, weight) = (neighbor_pair.node, neighbor_pair.weight);
                let new_dist = min_dist + weight;

                if new_dist < dists[&neighbor] {
                    dists.insert(neighbor, new_dist);
                    queue.push(NodeDistPair { node: neighbor, dist: new_dist });
                }
            }
        }

        dists
    }
}

pub fn dijkstra_assignment<P: AsRef<Path>>(path: P) -> String {
    let graph = Graph::from_file(path).unwrap();
    let dists = graph.dijkstra(1);

    let mut assign_dists: [Dist; 10] = [0; 10];
    for (i, node) in ASSIGN_NODES.iter().enumerate() {
        let dist = dists[node];
        assign_dists[i] = if dist < Dist::MAX {
            dist
        } else {
            ASSIGN_INFINITY
        };
    }

    assign_dists
        .iter()
        .map(|&d| d.to_string())
        .collect::<Vec<String>>()
        .join(",")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_graph_from_file_1() {
        let path = "text_files/problem9.8test.txt";

        let res = Graph::from_file(path);
        assert!(res.is_ok());
        let graph = res.unwrap();

        assert_eq!(graph.nodes.len(), 8);
        assert!((1..=8).all(|n| graph.nodes.contains(&n)));

        assert_eq!(graph.adj_list.len(), 8);
        assert!(graph.adj_list.keys().all(|&k| 1 <= k && k <= 8));

        let vec1 = vec![
            NodeWeightPair { node: 2, weight: 1 },
            NodeWeightPair { node: 8, weight: 2 },
        ];
        assert_eq!(graph.adj_list.get(&1), Some(&vec1));

        let vec8 = vec![
            NodeWeightPair { node: 7, weight: 1 },
            NodeWeightPair { node: 1, weight: 2 },
        ];
        assert_eq!(graph.adj_list.get(&8), Some(&vec8));
    }

    #[test]
    fn test_dijkstra_1() {
        let path = "text_files/problem9.8test.txt";

        let res = Graph::from_file(path);
        assert!(res.is_ok());
        let graph = res.unwrap();

        let source = 1;
        let dists = graph.dijkstra(source);

        assert_eq!(dists.get(&1), Some(&0));
        assert_eq!(dists.get(&2), Some(&1));
        assert_eq!(dists.get(&3), Some(&2));
        assert_eq!(dists.get(&4), Some(&3));
        assert_eq!(dists.get(&5), Some(&4));
        assert_eq!(dists.get(&6), Some(&4));
        assert_eq!(dists.get(&7), Some(&3));
        assert_eq!(dists.get(&8), Some(&2));
    }

    #[test]
    fn test_dijkstra_assignment_16_nodes() {
        let path = "text_files/input_random_12_16.txt";
        let result = dijkstra_assignment(path);
        let expected = String::from("1518,840,901,1303,1065,1122,612,1139,873,1018");
        assert_eq!(result, expected);
    }

    #[test]
    fn test_dijkstra_assignment_32_nodes() {
        let path = "text_files/input_random_16_32.txt";
        let result = dijkstra_assignment(path);
        let expected = String::from("10166,18051,15617,16074,16134,15292,17621,18248,15367,13089");
        assert_eq!(result, expected);
    }

    #[test]
    fn test_dijkstra_assignment_64_nodes() {
        let path = "text_files/input_random_19_64.txt";
        let result = dijkstra_assignment(path);
        let expected = String::from("79573,66019,97675,75567,81064,72571,51539,38800,67131,51512");
        assert_eq!(result, expected);
    }

    #[test]
    fn test_dijkstra_assignment_128_nodes() {
        let path = "text_files/input_random_24_128.txt";
        let result = dijkstra_assignment(path);
        let expected = String::from("28256,26397,28788,24491,48786,27993,29617,19807,40062,31045");
        assert_eq!(result, expected);
    }

    #[test]
    fn test_dijkstra_assignment_256_nodes() {
        let path = "text_files/input_random_25_256.txt";
        let result = dijkstra_assignment(path);
        let expected =
            String::from("359252,162056,186546,108269,184936,189604,199891,204410,179319,286061");
        assert_eq!(result, expected);
    }
}
