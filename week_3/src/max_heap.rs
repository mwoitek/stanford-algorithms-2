use num_traits::Bounded;

pub struct MaxHeap<T: Bounded + Clone + PartialOrd> {
    arr: Vec<T>,
    size: usize,
}

#[inline]
fn parent(i: usize) -> usize { (i - 1) / 2 }

#[inline]
fn left_child(i: usize) -> usize { 2 * i + 1 }

#[inline]
fn right_child(i: usize) -> usize { 2 * i + 2 }

impl<T: Bounded + Clone + PartialOrd> MaxHeap<T> {
    pub fn new() -> Self { Self { arr: Vec::new(), size: 0 } }

    pub fn len(&self) -> usize { self.size }

    pub fn is_empty(&self) -> bool { self.size == 0 }

    fn sift_down(&mut self, i: usize) {
        let mut largest = i;

        let left = left_child(i);
        if left < self.size && self.arr[left] > self.arr[i] {
            largest = left;
        }

        let right = right_child(i);
        if right < self.size && self.arr[right] > self.arr[largest] {
            largest = right;
        }

        if largest != i {
            self.arr.swap(largest, i);
            self.sift_down(largest);
        }
    }

    pub fn from_slice(a: &[T]) -> Self {
        if a.is_empty() {
            return Self::new();
        }
        let mut heap = Self { arr: a.to_vec(), size: a.len() };
        for i in (0..heap.size / 2).rev() {
            heap.sift_down(i);
        }
        heap
    }

    pub fn max(&self) -> Option<&T> { self.arr.first() }

    pub fn pop_max(&mut self) -> Option<T> {
        let max = self.arr.first().cloned()?;
        self.arr.swap(0, self.size - 1);
        self.arr.pop();
        self.size -= 1;
        self.sift_down(0);
        Some(max)
    }

    fn sift_up(&mut self, i: usize) {
        let mut j = i;
        while j > 0 {
            let p = parent(j);
            if self.arr[p] >= self.arr[j] {
                break;
            }
            self.arr.swap(j, p);
            j = p;
        }
    }

    pub fn push(&mut self, val: T) {
        self.arr.push(val);
        self.size += 1;
        self.sift_up(self.size - 1);
    }

    pub fn pop(&mut self, i: usize) -> Option<T> {
        let val = self.arr.get(i).cloned()?;
        self.arr[i] = T::max_value();
        self.sift_up(i);
        self.pop_max();
        Some(val)
    }
}

impl<T: Bounded + Clone + PartialOrd> Default for MaxHeap<T> {
    fn default() -> Self { Self::new() }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn is_max_heap<T: Bounded + Clone + PartialOrd>(heap: &MaxHeap<T>) -> bool {
        for i in 0..heap.size / 2 {
            let left = left_child(i);
            if left < heap.size && heap.arr[left] > heap.arr[i] {
                return false;
            }
            let right = right_child(i);
            if right < heap.size && heap.arr[right] > heap.arr[i] {
                return false;
            }
        }
        true
    }

    #[test]
    fn test_from_slice_1() {
        let v: Vec<u32> = vec![13, 16, 31, 41, 51, 100];
        let heap = MaxHeap::from_slice(&v);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![100, 51, 31, 41, 16, 13]);
    }

    #[test]
    fn test_from_slice_2() {
        let v: Vec<u32> = vec![4, 1, 3, 2, 16, 9, 10, 14, 8, 7];
        let heap = MaxHeap::from_slice(&v);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![16, 14, 10, 8, 7, 9, 3, 2, 4, 1]);
    }

    #[test]
    fn test_pop_max_2() {
        let v: Vec<u32> = vec![4, 1, 3, 2, 16, 9, 10, 14, 8, 7];
        let mut heap = MaxHeap::from_slice(&v);
        let opt = heap.pop_max();
        assert!(opt.is_some());
        let max = opt.unwrap();
        assert_eq!(max, 16);
        assert_eq!(heap.arr, vec![14, 8, 10, 4, 7, 9, 3, 2, 1]);
    }

    #[test]
    fn test_pop_max_empty_heap() {
        let v: Vec<i32> = vec![];
        let mut heap = MaxHeap::from_slice(&v);
        assert!(heap.pop_max().is_none());
    }

    #[test]
    fn test_push_1() {
        let v: Vec<u32> = vec![13, 16, 31, 41, 51, 100];
        let mut heap = MaxHeap::from_slice(&v);
        heap.push(20);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![100, 51, 31, 41, 16, 13, 20]);
    }

    #[test]
    fn test_push_2() {
        let v: Vec<u32> = vec![4, 1, 3, 2, 16, 9, 10, 14, 8, 7];
        let mut heap = MaxHeap::from_slice(&v);
        heap.push(15);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![16, 15, 10, 8, 14, 9, 3, 2, 4, 1, 7]);
    }

    #[test]
    fn test_pop_from_front() {
        let v: Vec<u32> = vec![50, 30, 20, 15, 10, 8];
        let mut heap = MaxHeap::from_slice(&v);
        let opt = heap.pop(0);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 50);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![30, 15, 20, 8, 10]);
    }

    #[test]
    fn test_pop_from_middle() {
        let v: Vec<u32> = vec![100, 51, 31, 41, 16, 13];
        let mut heap = MaxHeap::from_slice(&v);
        let opt = heap.pop(3);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 41);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![100, 51, 31, 13, 16]);
    }

    #[test]
    fn test_pop_from_back() {
        let v: Vec<u32> = vec![50, 30, 20, 15, 10, 8, 16];
        let mut heap = MaxHeap::from_slice(&v);
        let opt = heap.pop(6);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 16);
        assert!(is_max_heap(&heap));
        assert_eq!(heap.arr, vec![50, 30, 20, 15, 10, 8]);
    }

    #[test]
    fn test_pop_empty_heap() {
        let v: Vec<u32> = vec![];
        let mut heap = MaxHeap::from_slice(&v);
        assert!(heap.pop(1).is_none());
    }

    #[test]
    fn test_pop_invalid_index() {
        let v: Vec<u32> = vec![100, 51, 31, 41, 16, 13];
        let mut heap = MaxHeap::from_slice(&v);
        assert!(heap.pop(100).is_none());
    }
}
