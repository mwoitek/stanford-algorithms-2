use super::max_heap::MaxHeap;
use super::min_heap::MinHeap;

use num_traits::Bounded;

pub struct MedianHeap<T: Bounded + Clone + PartialOrd> {
    small: MaxHeap<T>,
    large: MinHeap<T>,
}

impl<T: Bounded + Clone + PartialOrd> MedianHeap<T> {
    pub fn new() -> Self {
        Self {
            small: MaxHeap::new(),
            large: MinHeap::new(),
        }
    }

    fn balance(&mut self) {
        let max_size = self.small.len();
        let min_size = self.large.len();
        if max_size > min_size && max_size - min_size == 2 {
            let max = self.small.pop_max().unwrap();
            self.large.push(max);
        } else if min_size > max_size && min_size - max_size == 2 {
            let min = self.large.pop_min().unwrap();
            self.small.push(min);
        }
    }

    pub fn median(&self) -> Option<&T> {
        if self.small.len() >= self.large.len() {
            self.small.max()
        } else {
            self.large.min()
        }
    }

    pub fn push(&mut self, val: T) {
        if let Some(med) = self.median() {
            if val > *med {
                self.large.push(val);
            } else {
                self.small.push(val);
            }
            self.balance();
        } else {
            self.small.push(val);
        }
    }
}

impl<T: Bounded + Clone + PartialOrd> Default for MedianHeap<T> {
    fn default() -> Self { Self::new() }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_median_seq_1() {
        let mut heap: MedianHeap<u32> = MedianHeap::new();
        let vals: Vec<u32> = vec![1, 2, 3];
        let mut medians: Vec<u32> = Vec::with_capacity(vals.len());
        for val in vals {
            heap.push(val);
            let median = heap.median().unwrap();
            medians.push(*median);
        }
        assert_eq!(medians, vec![1, 1, 2]);
    }

    #[test]
    fn test_median_seq_2() {
        let mut heap: MedianHeap<i32> = MedianHeap::new();
        let vals: Vec<i32> = vec![10, 2, -3, 4, -1, 0];
        let mut medians: Vec<i32> = Vec::with_capacity(vals.len());
        for val in vals {
            heap.push(val);
            let median = heap.median().unwrap();
            medians.push(*median);
        }
        assert_eq!(medians, vec![10, 2, 2, 2, 2, 0]);
    }

    #[test]
    fn test_median_seq_3() {
        let mut heap: MedianHeap<f64> = MedianHeap::new();
        let vals: Vec<f64> = vec![1.2, 0.3, 2.2, 1.1, 0.4];
        let mut medians: Vec<f64> = Vec::with_capacity(vals.len());
        for val in vals {
            heap.push(val);
            let median = heap.median().unwrap();
            medians.push(*median);
        }
        assert_eq!(medians, vec![1.2, 0.3, 1.2, 1.1, 1.1]);
    }

    #[test]
    fn test_median_empty_heap() {
        let heap: MedianHeap<u8> = MedianHeap::new();
        assert!(heap.median().is_none());
    }
}
