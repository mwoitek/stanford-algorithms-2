use num_traits::Bounded;

pub struct MinHeap<T: Bounded + Clone + PartialOrd> {
    arr: Vec<T>,
    size: usize,
}

#[inline]
fn parent(i: usize) -> usize { (i - 1) / 2 }

#[inline]
fn left_child(i: usize) -> usize { 2 * i + 1 }

#[inline]
fn right_child(i: usize) -> usize { 2 * i + 2 }

impl<T: Bounded + Clone + PartialOrd> MinHeap<T> {
    pub fn new() -> Self { Self { arr: Vec::new(), size: 0 } }

    pub fn len(&self) -> usize { self.size }

    pub fn is_empty(&self) -> bool { self.size == 0 }

    fn sift_down(&mut self, i: usize) {
        let mut smallest = i;

        let left = left_child(i);
        if left < self.size && self.arr[left] < self.arr[i] {
            smallest = left;
        }

        let right = right_child(i);
        if right < self.size && self.arr[right] < self.arr[smallest] {
            smallest = right;
        }

        if smallest != i {
            self.arr.swap(smallest, i);
            self.sift_down(smallest);
        }
    }

    pub fn from_slice(a: &[T]) -> Self {
        if a.is_empty() {
            return Self::new();
        }
        let mut heap = Self { arr: a.to_vec(), size: a.len() };
        for i in (0..heap.size / 2).rev() {
            heap.sift_down(i);
        }
        heap
    }

    pub fn min(&self) -> Option<&T> { self.arr.first() }

    pub fn pop_min(&mut self) -> Option<T> {
        let min = self.arr.first().cloned()?;
        self.arr.swap(0, self.size - 1);
        self.arr.pop();
        self.size -= 1;
        self.sift_down(0);
        Some(min)
    }

    fn sift_up(&mut self, i: usize) {
        let mut j = i;
        while j > 0 {
            let p = parent(j);
            if self.arr[p] <= self.arr[j] {
                break;
            }
            self.arr.swap(j, p);
            j = p;
        }
    }

    pub fn push(&mut self, val: T) {
        self.arr.push(val);
        self.size += 1;
        self.sift_up(self.size - 1);
    }

    pub fn pop(&mut self, i: usize) -> Option<T> {
        let val = self.arr.get(i).cloned()?;
        self.arr[i] = T::min_value();
        self.sift_up(i);
        self.pop_min();
        Some(val)
    }
}

impl<T: Bounded + Clone + PartialOrd> Default for MinHeap<T> {
    fn default() -> Self { Self::new() }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn is_min_heap<T: Bounded + Clone + PartialOrd>(heap: &MinHeap<T>) -> bool {
        for i in 0..heap.size / 2 {
            let left = left_child(i);
            if left < heap.size && heap.arr[left] < heap.arr[i] {
                return false;
            }
            let right = right_child(i);
            if right < heap.size && heap.arr[right] < heap.arr[i] {
                return false;
            }
        }
        true
    }

    #[test]
    fn test_from_slice_1() {
        let v: Vec<u32> = vec![51, 16, 100, 41, 13, 31];
        let heap = MinHeap::from_slice(&v);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![13, 16, 31, 41, 51, 100]);
    }

    #[test]
    fn test_from_slice_2() {
        let v: Vec<u32> = vec![4, 1, 3, 2, 16, 9, 10, 14, 8, 7];
        let heap = MinHeap::from_slice(&v);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![1, 2, 3, 4, 7, 9, 10, 14, 8, 16]);
    }

    #[test]
    fn test_pop_min_2() {
        let v: Vec<u32> = vec![4, 1, 3, 2, 16, 9, 10, 14, 8, 7];
        let mut heap = MinHeap::from_slice(&v);
        let opt = heap.pop_min();
        assert!(opt.is_some());
        let min = opt.unwrap();
        assert_eq!(min, 1);
        assert_eq!(heap.arr, vec![2, 4, 3, 8, 7, 9, 10, 14, 16]);
    }

    #[test]
    fn test_pop_min_empty_heap() {
        let v: Vec<i32> = vec![];
        let mut heap = MinHeap::from_slice(&v);
        assert!(heap.pop_min().is_none());
    }

    #[test]
    fn test_push_1() {
        let v: Vec<u32> = vec![13, 16, 31, 41, 51, 100];
        let mut heap = MinHeap::from_slice(&v);
        heap.push(20);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![13, 16, 20, 41, 51, 100, 31]);
    }

    #[test]
    fn test_push_2() {
        let v: Vec<u32> = vec![2, 4, 3, 8, 7, 9, 10, 14, 16];
        let mut heap = MinHeap::from_slice(&v);
        heap.push(0);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![0, 2, 3, 8, 4, 9, 10, 14, 16, 7]);
    }

    #[test]
    fn test_pop_from_front() {
        let v: Vec<u32> = vec![11, 12, 15, 24, 14, 22, 30];
        let mut heap = MinHeap::from_slice(&v);
        let opt = heap.pop(0);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 11);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![12, 14, 15, 24, 30, 22]);
    }

    #[test]
    fn test_pop_from_middle() {
        let v: Vec<u32> = vec![10, 20, 30, 40, 50];
        let mut heap = MinHeap::from_slice(&v);
        let opt = heap.pop(2);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 30);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![10, 20, 50, 40]);
    }

    #[test]
    fn test_pop_from_back() {
        let v: Vec<u32> = vec![11, 12, 15, 24, 14, 22, 30];
        let mut heap = MinHeap::from_slice(&v);
        let opt = heap.pop(6);
        assert!(opt.is_some());
        assert_eq!(opt.unwrap(), 30);
        assert!(is_min_heap(&heap));
        assert_eq!(heap.arr, vec![11, 12, 15, 24, 14, 22]);
    }

    #[test]
    fn test_pop_empty_heap() {
        let v: Vec<u32> = vec![];
        let mut heap = MinHeap::from_slice(&v);
        assert!(heap.pop(1).is_none());
    }

    #[test]
    fn test_pop_invalid_index() {
        let v: Vec<u32> = vec![100, 51, 31, 41, 16, 13];
        let mut heap = MinHeap::from_slice(&v);
        assert!(heap.pop(100).is_none());
    }
}
