pub mod max_heap;
pub mod median_heap;
pub mod min_heap;

use median_heap::MedianHeap;
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::num::ParseIntError;
use std::path::Path;

/// Custom error type to handle different kinds of errors
/// when reading a text file with integers.
#[derive(Debug)]
pub enum ReadError {
    Io(io::Error),
    Parse(ParseIntError),
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> ReadError { ReadError::Io(err) }
}

impl From<ParseIntError> for ReadError {
    fn from(err: ParseIntError) -> ReadError { ReadError::Parse(err) }
}

impl fmt::Display for ReadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ReadError::Io(err) => write!(f, "IO error: {}", err),
            ReadError::Parse(err) => write!(f, "Parse error: {}", err),
        }
    }
}

/// Function to read integers from a file.
pub fn read_integers<P: AsRef<Path>>(path: P) -> Result<Vec<i64>, ReadError> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let mut ints: Vec<i64> = Vec::new();
    for line in reader.lines().map(|l| l.unwrap()) {
        let int: i64 = line.trim().parse()?;
        ints.push(int);
    }
    Ok(ints)
}

pub fn median_sum(vals: &Vec<i64>) -> i64 {
    let mut sum = 0;
    let mut heap: MedianHeap<i64> = MedianHeap::new();
    for &val in vals {
        heap.push(val);
        let median = heap.median().unwrap();
        sum = (sum + *median) % 10000;
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_median_sum_1() {
        let vals: Vec<i64> = vec![6331, 2793, 1640, 9290, 225, 625, 6195, 2303, 5685, 1354];
        let sum = median_sum(&vals);
        assert_eq!(sum, 9335);
    }

    #[test]
    fn test_median_sum_2() {
        let vals: Vec<i64> = vec![
            6, 8, 3, 10, 7, 11, 19, 16, 12, 9, 2, 15, 1, 20, 13, 5, 18, 14, 4, 17,
        ];
        let sum = median_sum(&vals);
        assert_eq!(sum, 168);
    }

    #[test]
    fn test_median_sum_3() {
        let vals: Vec<i64> = vec![
            4, 5, 13, 1, 2, 16, 18, 6, 20, 11, 15, 3, 17, 8, 10, 9, 19, 14, 12, 7,
        ];
        let sum = median_sum(&vals);
        assert_eq!(sum, 143);
    }

    #[test]
    fn test_median_sum_4() {
        let vals: Vec<i64> = vec![
            8, 37, 33, 34, 18, 35, 7, 2, 14, 10, 36, 39, 26, 22, 24, 25, 17, 29, 38, 30, 12, 15, 4,
            32, 23, 40, 1, 28, 20, 19, 6, 9, 27, 11, 31, 21, 3, 5, 13, 16,
        ];
        let sum = median_sum(&vals);
        assert_eq!(sum, 920);
    }
}
