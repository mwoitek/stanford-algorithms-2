Stanford Algorithms Part 2
==========================

This repo contains solutions to the programming assignments of the course
"Graph Search, Shortest Paths, and Data Structures". This course is offered
on [Coursera](https://www.coursera.org/learn/algorithms-graphs-data-structures)
by Stanford University.

All the solutions were implemented in Rust. In fact, I decided to take this
course to become a better Rustacean. So don't expect expert Rust code. My
code is pretty decent though.

This repo is organized as follows. For every week, I created a library crate.
These crates contain the actual assignment code. I also created a binary crate
named "answers". As the name suggests, this crate can be used to obtain the
assignment answers. To do so, clone this repo and in its root directory use the
following command:
```
cargo run -r
```

If you just want to check out my solutions, you can follow one of the links below:
- [Strongly connected components](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_1/src/lib.rs)
- [Dijkstra's algorithm](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_2/src/lib.rs)
- [Median maintenance problem](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_3/src/lib.rs)
  - [Min heap](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_3/src/min_heap.rs)
  - [Max heap](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_3/src/max_heap.rs)
  - [Median heap](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_3/src/median_heap.rs)
- [2-SUM problem](https://gitlab.com/mwoitek/stanford-algorithms-2/-/blob/master/week_4/src/lib.rs)
