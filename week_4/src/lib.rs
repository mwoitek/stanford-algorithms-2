//! Week 4: Programming Assignment
//!
//! 2-SUM

use rayon::prelude::*;
use rustc_hash::FxHashSet;

pub type TwoSumFn = fn(&[i64], i64) -> bool;

/// Naive solution to the 2-SUM problem.
pub fn two_sum_naive(ints: &[i64], target: i64) -> bool {
    let size = ints.len();
    for i in 0..size - 1 {
        let diff = target - ints[i];
        for j in i + 1..size {
            if ints[j] == diff && ints[j] != ints[i] {
                return true;
            }
        }
    }
    false
}

/// Solution to 2-SUM that uses a HashSet and 2 passes.
pub fn two_sum_hash2(ints: &[i64], target: i64) -> bool {
    // 1st pass
    let set: FxHashSet<i64> = ints.iter().cloned().collect();
    // 2nd pass
    two_sum_with_hashset(ints, target, &set)
}

fn two_sum_with_hashset(ints: &[i64], target: i64, set: &FxHashSet<i64>) -> bool {
    for &int in ints {
        let diff = target - int;
        if set.contains(&diff) && diff != int {
            return true;
        }
    }
    false
}

/// Solution to 2-SUM that uses a HashSet and a single pass.
pub fn two_sum_hash1(ints: &[i64], target: i64) -> bool {
    let mut set: FxHashSet<i64> = FxHashSet::default();
    for &int in ints {
        let diff = target - int;
        if set.contains(&diff) && diff != int {
            return true;
        }
        set.insert(int);
    }
    false
}

/// Solution to 2-SUM that uses 2 pointers.
pub fn two_sum_2pointers(ints: &mut [i64], target: i64) -> bool {
    ints.sort_unstable();

    let mut i = 0;
    let mut j = ints.len() - 1;

    while i < j {
        let sum = ints[i] + ints[j];
        if sum == target && ints[i] != ints[j] {
            return true;
        }
        if sum < target {
            i += 1;
        } else {
            j -= 1;
        }
    }

    false
}

/// Compute the number of targets belonging to [target_min, target_max]
/// for which the corresponding 2-SUM problem has a solution. Runs sequentially.
pub fn count_targets_seq(ints: &[i64], target_interval: (i64, i64), func: TwoSumFn) -> usize {
    let (target_min, target_max) = target_interval;
    (target_min..=target_max).filter(|&t| func(ints, t)).count()
}

/// Compute the number of targets belonging to [target_min, target_max]
/// for which the corresponding 2-SUM problem has a solution. Runs in parallel.
pub fn count_targets_par(ints: &[i64], target_interval: (i64, i64)) -> usize {
    let (target_min, target_max) = target_interval;
    let set: FxHashSet<i64> = ints.iter().cloned().collect();
    (target_min..=target_max)
        .into_par_iter()
        .filter(|&t| two_sum_with_hashset(ints, t, &set))
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;
    use week_3::read_integers;

    #[test]
    fn test_naive_has_solution() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target = 6;
        assert!(two_sum_naive(&ints, target));
    }

    #[test]
    fn test_naive_no_solution() {
        let ints: Vec<i64> = vec![3, 5, 7, 9, 11, 13, 15];
        let target = 30;
        assert!(!two_sum_naive(&ints, target));
    }

    #[test]
    fn test_naive_has_solution_only_equal_values() {
        let ints: Vec<i64> = vec![6, 4, 3, 2, 1, 6, 12];
        let target = 12;
        assert!(!two_sum_naive(&ints, target));
    }

    #[test]
    fn test_hash2_has_solution() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target = 6;
        assert!(two_sum_hash2(&ints, target));
    }

    #[test]
    fn test_hash2_no_solution() {
        let ints: Vec<i64> = vec![3, 5, 7, 9, 11, 13, 15];
        let target = 30;
        assert!(!two_sum_hash2(&ints, target));
    }

    #[test]
    fn test_hash2_has_solution_only_equal_values() {
        let ints: Vec<i64> = vec![6, 4, 3, 2, 1, 6, 12];
        let target = 12;
        assert!(!two_sum_hash2(&ints, target));
    }

    #[test]
    fn test_hash1_has_solution() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target = 6;
        assert!(two_sum_hash1(&ints, target));
    }

    #[test]
    fn test_hash1_no_solution() {
        let ints: Vec<i64> = vec![3, 5, 7, 9, 11, 13, 15];
        let target = 30;
        assert!(!two_sum_hash1(&ints, target));
    }

    #[test]
    fn test_hash1_has_solution_only_equal_values() {
        let ints: Vec<i64> = vec![6, 4, 3, 2, 1, 6, 12];
        let target = 12;
        assert!(!two_sum_hash1(&ints, target));
    }

    #[test]
    fn test_2pointers_has_solution() {
        let mut ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target = 6;
        assert!(two_sum_2pointers(&mut ints, target));
    }

    #[test]
    fn test_2pointers_no_solution() {
        let mut ints: Vec<i64> = vec![3, 5, 7, 9, 11, 13, 15];
        let target = 30;
        assert!(!two_sum_2pointers(&mut ints, target));
    }

    #[test]
    fn test_2pointers_has_solution_only_equal_values() {
        let mut ints: Vec<i64> = vec![6, 4, 3, 2, 1, 6, 12];
        let target = 12;
        assert!(!two_sum_2pointers(&mut ints, target));
    }

    #[test]
    fn test_count_naive_1() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (1, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_naive);
        assert_eq!(count, 6);
    }

    #[test]
    fn test_count_naive_2() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (-10, 1);
        let count = count_targets_seq(&ints, target_interval, two_sum_naive);
        assert_eq!(count, 0);
    }

    #[test]
    fn test_count_naive_3() {
        let ints: Vec<i64> = vec![-3, -1, 1, 2, 9, 11, 7, 6, 2];
        let target_interval: (i64, i64) = (3, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_naive);
        assert_eq!(count, 8);
    }

    #[test]
    fn test_count_hash2_1() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (1, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash2);
        assert_eq!(count, 6);
    }

    #[test]
    fn test_count_hash2_2() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (-10, 1);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash2);
        assert_eq!(count, 0);
    }

    #[test]
    fn test_count_hash2_3() {
        let ints: Vec<i64> = vec![-3, -1, 1, 2, 9, 11, 7, 6, 2];
        let target_interval: (i64, i64) = (3, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash2);
        assert_eq!(count, 8);
    }

    #[test]
    fn test_count_hash1_1() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (1, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 6);
    }

    #[test]
    fn test_count_hash1_2() {
        let ints: Vec<i64> = vec![2, 3, 4, 5, 6];
        let target_interval: (i64, i64) = (-10, 1);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 0);
    }

    #[test]
    fn test_count_hash1_3() {
        let ints: Vec<i64> = vec![-3, -1, 1, 2, 9, 11, 7, 6, 2];
        let target_interval: (i64, i64) = (3, 10);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 8);
    }

    #[test]
    #[ignore]
    fn test_count_seq_160_integers() {
        let path = "text_files/input_random_17_160.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 20);
    }

    #[test]
    #[ignore]
    fn test_count_seq_640_integers() {
        let path = "text_files/input_random_27_640.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 31);
    }

    #[test]
    #[ignore]
    fn test_count_seq_1280_integers() {
        let path = "text_files/input_random_29_1280.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 51);
    }

    #[test]
    #[ignore]
    fn test_count_seq_2560_integers() {
        let path = "text_files/input_random_36_2560.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 78);
    }

    #[test]
    #[ignore]
    fn test_count_seq_5120_integers() {
        let path = "text_files/input_random_37_5120.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_seq(&ints, target_interval, two_sum_hash1);
        assert_eq!(count, 96);
    }

    #[test]
    #[ignore]
    fn test_count_par_160_integers() {
        let path = "text_files/input_random_17_160.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_par(&ints, target_interval);
        assert_eq!(count, 20);
    }

    #[test]
    #[ignore]
    fn test_count_par_640_integers() {
        let path = "text_files/input_random_27_640.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_par(&ints, target_interval);
        assert_eq!(count, 31);
    }

    #[test]
    #[ignore]
    fn test_count_par_1280_integers() {
        let path = "text_files/input_random_29_1280.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_par(&ints, target_interval);
        assert_eq!(count, 51);
    }

    #[test]
    #[ignore]
    fn test_count_par_2560_integers() {
        let path = "text_files/input_random_36_2560.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_par(&ints, target_interval);
        assert_eq!(count, 78);
    }

    #[test]
    #[ignore]
    fn test_count_par_5120_integers() {
        let path = "text_files/input_random_37_5120.txt";
        let ints = read_integers(path).unwrap();
        let target_interval: (i64, i64) = (-10000, 10000);
        let count = count_targets_par(&ints, target_interval);
        assert_eq!(count, 96);
    }
}
