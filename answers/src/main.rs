use week_2::dijkstra_assignment;
use week_4::count_targets_par;

fn main() {
    println!("Programming Assignments: Answers\n");
    assignment_1();
    println!();
    assignment_2();
    println!();
    assignment_3();
    println!();
    assignment_4();
}

fn assignment_1() {
    println!("Assignment 1");

    // NOTE: This file is NOT in the repo!
    let path = "week_1/text_files/SCC.txt";

    match week_1::Graph::from_file(path) {
        Ok(graph) => {
            let sizes = graph.top_component_sizes();
            let answer = sizes
                .iter()
                .map(|&s| s.to_string())
                .collect::<Vec<String>>()
                .join(",");
            println!("Answer: {}", answer); // 434821,968,459,313,211
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}

fn assignment_2() {
    println!("Assignment 2");
    let path = "week_2/text_files/dijkstraData.txt";
    let answer = dijkstra_assignment(path);
    println!("Answer: {}", answer); // 2599,2610,2947,2052,2367,2399,2029,2442,2505,3068
}

fn assignment_3() {
    println!("Assignment 3");
    let path = "week_3/text_files/Median.txt";
    match week_3::read_integers(path) {
        Ok(ints) => {
            let sum = week_3::median_sum(&ints);
            println!("Answer: {}", sum); // 1213
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}

fn assignment_4() {
    println!("Assignment 4");

    // NOTE: This file is NOT in the repo!
    let path = "week_4/text_files/algo1-programming_prob-2sum.txt";

    match week_3::read_integers(path) {
        Ok(ints) => {
            let target_interval = (-10000, 10000);
            let count = count_targets_par(&ints, target_interval);
            println!("Answer: {}", count); // 427
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}
